#!/bin/bash

/usr/bin/entrypoint.sh &

while ! pgrep -f fluentd 2>&1 > /dev/null
do
    echo "Waiting for fluent..."
    sleep 5
done

python2 /app/oai/oai-harvest.py $* &> /app/oai/oai-harvest.log

sleep 5
